function apply() {
    var wallet = $('#wallet').val();
    $.cookie('wallet', wallet, {expires: 120, path: '/'});
}

function loadLookUp() {
    var wallet = $.cookie('wallet');
    if (wallet) {
        $('#wallet').val(wallet);
        $.cookie('wallet', wallet, {expires: 120, path: '/'});
    }
    lookUp();
}

function lookUp(options) {
    if ($.cookie('wallet')) {
        var url = 'http://pool.bytecoin.party:1818/stats_address?longpoll=false&address=' + $.cookie('wallet');
        $.ajax(url, {
                success: function (response) {
                    if (!options) {
                        fillUserStats(response.stats);
                        fillRecentPayouts(response.payments);
                        fillHashRate(response.charts.hashrate);
                    } else {
                        if (options.userStats) fillUserStats(response.stats);
                        if (options.recentPayouts) fillRecentPayouts(response.payments);
                        if (options.hashRates) fillHashRates(response.charts.hashrate);
                    }
                }
            }
        );
    }
}