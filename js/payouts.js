function fillRecentPayouts(payments) {
    var lastDay = null;
    var item = '<li class="list-group-item">{0} <a target="_blank" href="{1}">##</a><b>{2} BCN</b></li>';
    var trxUrl = 'http://chainradar.com/bcn/transaction/{0}';
    var payouts = $('#payouts');
    payouts.empty();
    for (var i = 1; i < payments.length; i++) {
        if (i % 2 !== 0) {
            var date = new Date();
            date.setTime(payments[i] * 1000);
            if (lastDay !== null && lastDay !== date.getDate()) {
                break;
            } else {
                lastDay = date.getDate();
                var parts = payments[i - 1].split(':');
                date = date.toLocaleString('en-US', {timeZone: 'Asia/Beirut'}).rpad(22, ' ');
                var bcn = (parts[1] / parts[2] + '').lpad(4);
                var label = item.format(date, trxUrl.format(parts[0]), bcn);
                payouts.append($(label));
            }
        }
    }
}