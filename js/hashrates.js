function fillHashRate(rates) {

    var dataPoints = [];
    var labels = [];
    var max = 0;

    $.map(rates, function (t) {
        var hashRate = t[1];
        if (hashRate > max)
            max = hashRate;
        dataPoints.push(hashRate);
        labels.push(t[0] * 1000);
    });

    /*
    hide the clickable legend
     */
    Chart.defaults.global.legend = false;

    Chart.defaults.LineWithLine = Chart.defaults.line;
    Chart.controllers.LineWithLine = Chart.controllers.line.extend({
        draw: function (ease) {
            Chart.controllers.line.prototype.draw.call(this, ease);
            if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
                var activePoint = this.chart.tooltip._active[0],
                    ctx = this.chart.ctx,
                    x = activePoint.tooltipPosition().x,
                    topY = this.chart.scales['y-axis-0'].top,
                    bottomY = this.chart.scales['y-axis-0'].bottom;

                // draw line
                ctx.save();
                ctx.beginPath();
                ctx.moveTo(x, topY);
                ctx.lineTo(x, bottomY);
                ctx.lineWidth = 1;
                ctx.strokeStyle = '#0f0';
                ctx.stroke();
                ctx.restore();
            }
        }
    });

    /*
    custom tooltip function
     */
    var customTooltips = function (tooltip) {

        $(this._chart.canvas).css("cursor", "pointer");

        var positionY = this._chart.canvas.offsetTop;
        var positionX = this._chart.canvas.offsetLeft;

        $(".chartjs-tooltip").css({
            opacity: 0
        });

        if (!tooltip || !tooltip.opacity) {
            return;
        }

        if (tooltip.dataPoints.length > 0) {
            tooltip.dataPoints.forEach(function (dataPoint) {

                var date = new Date(dataPoint.xLabel).toLocaleTimeString();
                var content = [date, dataPoint.yLabel.format() + ' H/s'].join(" - ");
                var $tooltip = $("#canvas-tooltip");
                $tooltip.html(content);
                $tooltip.css({
                    opacity: 1,
                    top: positionY - 68 + "px",
                    right: -68,
                    minWidth: '180px'
                });
            });
        }
    };

    var config = {
        type: 'LineWithLine',
        data: {
            labels: labels,
            datasets: [{
                label: "Total HashRate",
                data: dataPoints,
                borderColor: window.chartColors.blue,
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: 'Total HashRate'
            },
            tooltips: {
                enabled: false,
                mode: 'index',
                intersect: false,
                custom: customTooltips
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'HashRate'
                    },
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: max * 1.2
                    }
                }]
            }
        }
    };

    if (!window.myLine) {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    } else {

        // var indexToUpdate = Math.round(Math.random() * window.myLine.data.labels.length);
        // window.myLine.data.datasets[0].data[indexToUpdate] = 2600;

        window.myLine.options.animation.duration = 0;
        window.myLine.update();
    }
}