function formatBalance(balance) {
    balance = balance + '';
    if (balance.length > 8) {
        return balance.substring(0, balance.length - 8) + '.' + balance.substring(balance.length - 8) + ' ';
    } else {
        return balance + ' ';
    }
}

function formatPaid(paid) {
    return paid / 100000000 + '';
}


function fillUserStats(stats) {

    var balance = formatBalance(stats.balance);
    var paid = formatPaid(stats.paid) + ' BCN';
    var dt = new Date();
    dt.setTime(stats.lastShare * 1000);
    var time = new Date().getTime() - dt.getTime();
    time = (time / 1000).toFixed(2) + ' s ago';

    $('#statBalance').html(balance);
    $('#statTotalPaid').html(paid);
    $('#statHashRate').html(stats.hashrate);
    $('#statLastShare').html(time);
    $('#statTotalHash').html(Number(stats.hashes).format());
}