function task() {
    loadLookUp();
    callRigs();
    setTimeout(task, 3000);
}

$(function () {
    loadLookUp();
    loadRigs();
    task();
});

