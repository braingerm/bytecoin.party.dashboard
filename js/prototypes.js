/*
 * String.format("{0} - {1}", 2,4)
 */
if (!String.prototype.format) {
    String.prototype.format = function () {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };
}

/**
 * left padding
 */
if (!String.prototype.lpad) {
    String.prototype.lpad = function (length, pad) {
        if (length < this.length) return this;

        pad = pad || ' ';
        var str = this;

        while (str.length < length) {
            str = pad + str;
        }

        return str.substr(-length);
    };
}

/**
 * right padding
 */
if (!String.prototype.rpad) {
    String.prototype.rpad = function (length, pad) {
        if (length < this.length) return this;

        pad = pad || ' ';
        var str = this;

        while (str.length < length) {
            str += pad;
        }

        return str.substr(0, length);
    };
}

/*
 * browser timezone
 */
if (!Date.prototype.timezone) {
    Date.prototype.timezone = function () {
        this.setHours(this.getHours() + (new Date().getTimezoneOffset() / 60));
        return this;
    };
}

/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param number n: length of decimal
 * @param number x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
if (!Number.prototype.format) {
    Number.prototype.format = function (n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
}

/**
 * get the viewPort width, height
 * @returns {{x: (Number|number), y: (Number|number)}}
 */
if (!window.screenSize) {
    window.screenSize = function () {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;
        return {
            width: x, height: y
        };
    };
}