var rigs = null;

function getRigs() {
    if (rigs === null) {
        var json = $.cookie('rigs');
        if (json) {
            $.cookie('rigs', json, {expires: 120, path: '/'});
            rigs = JSON.parse(json);
        } else {
            rigs = [];
        }
    }
}

function saveRigs() {
    if (rigs !== null) {
        $.cookie('rigs', JSON.stringify(rigs), {expires: 120, path: '/'});
    }
}

function addRig() {

    var alias = $('#rigAlias').val();
    var endpoint = $('#rigEndpoint').val();

    if (!alias || !endpoint)
        return;

    getRigs();

    rigs.push({endpoint: endpoint, alias: alias, id: rigs.length});

    saveRigs();

    populateRigs();

}

function deleteRig(id) {
    for (var i = 0; i < rigs.length; i++)
        if (rigs[i].id === id) {
            rigs.splice(i, 1);
            break;
        }

    saveRigs();
    populateRigs();
}

function populateRigs() {

    var list = $('#rigs');
    list.empty();

    var template = '<tr>                                                                          ' +
        '    <th scope="row">{0}</th>                                                             ' +
        '    <td>{1}</td>                                                                         ' +
        '    <td>{2}</td>                                                                         ' +
        '    <td id="rig-hash-{3}">{4}H/s</td>                                                    ' +
        '    <td>                                                                                 ' +
        '        <span style="cursor:pointer;font-size: 12px" onclick="deleteRig({5})">&times</button>' +
        '    </td>                                                                                ' +
        '</tr>                                                                                    ';


    for (var i = 0; i < rigs.length; i++) {
        var rig = rigs[i];
        var item = template.format(i + 1, rig.endpoint, rig.alias, rig.id, ' ', rig.id);
        list.append($(item));
    }
}

function loadRigs() {
    getRigs();
    populateRigs();
    callRigs();
}

function ajax(url, id) {
    return $.ajax(url, {
        headers: {
            Authorization: ' Bearer franc'
        },
        success: function (response) {
            $('#rig-hash-' + id).html(response.hashrate.total[0] + 'H/s');
        }, error: function (error) {

        }
    });
}

function callRigs() {
    for (var i = 0; i < rigs.length; i++) {
        var url = 'http://' + rigs[i].endpoint + '/endpoint';
        var rig = rigs[i];
        ajax(url, rig.id)
    }
}